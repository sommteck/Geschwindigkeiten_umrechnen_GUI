using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Geschwindigkeiten_umrechnen_GUI
{
    public partial class Geschwindigkeiten_umrechnen : Form
    {
        public Geschwindigkeiten_umrechnen()
        {
            InitializeComponent();

            ToolTip help = new ToolTip();
            help.SetToolTip(txt_untergrenze, "Startwert eingeben");
            help.SetToolTip(txt_obergrenze, "Endwert eingeben");
            help.SetToolTip(txt_schrittweite, "Schrittweite eingeben");
            help.SetToolTip(cmd_umrechnen, "Umrechnung starten");
            help.SetToolTip(cmd_clear, "Ein- und Ausgaben löschen");
            help.SetToolTip(cmd_end, "Programm beenden");
            help.SetToolTip(comboBox1, "Umzuwandelte Einheit wählen");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_untergrenze.Text = txt_obergrenze.Text = txt_schrittweite.Text = null;
            dataGridView1.Rows.Clear();
        }

        private void cmd_umrechnen_Click(object sender, EventArgs e)
        {
            try
            {
                double untergenze, obergrenze, schrittweite, ms, kmh, mph, sm;

                dataGridView1.Rows.Clear();

                untergenze = Convert.ToDouble(txt_untergrenze.Text);
                obergrenze = Convert.ToDouble(txt_obergrenze.Text);
                schrittweite = Convert.ToDouble(txt_schrittweite.Text);

                if (comboBox1.Text == "m/s")
                {
                    for (; untergenze <= obergrenze; untergenze = untergenze + schrittweite)
                    {
                        ms = untergenze;
                        kmh = untergenze * 3.6;
                        mph = untergenze * 3.6 / 1.609344;
                        sm = untergenze * 3.6 / 1.852;

                        dataGridView1.Rows.Add(ms.ToString("F2"), kmh.ToString("F2"), mph.ToString("F2"), sm.ToString("F2"));
                    }
                }

                if (comboBox1.Text == "km/h")
                {
                    for (; untergenze <= obergrenze; untergenze = untergenze + schrittweite)
                    {
                        ms = untergenze / 3.6;
                        kmh = untergenze;
                        mph = untergenze / 1.609344;
                        sm = untergenze / 1.852;

                        dataGridView1.Rows.Add(ms.ToString("F2"), kmh.ToString("F2"), mph.ToString("F2"), sm.ToString("F2"));
                    }
                }

                if (comboBox1.Text == "mph")
                {
                    for (; untergenze <= obergrenze; untergenze = untergenze + schrittweite)
                    {
                        ms = untergenze * 1.609344 / 3.6;
                        kmh = untergenze * 1.609344;
                        mph = untergenze;
                        sm = untergenze * 1.609344 / 1.852;

                        dataGridView1.Rows.Add(ms.ToString("F2"), kmh.ToString("F2"), mph.ToString("F2"), sm.ToString("F2"));
                    }
                }

                if (comboBox1.Text == "sm")
                {
                    for (; untergenze <= obergrenze; untergenze = untergenze + schrittweite)
                    {
                        ms = untergenze * 1.852 / 3.6;
                        kmh = untergenze * 1.852;
                        mph = untergenze * 1.852 / 1.609344;
                        sm = untergenze;

                        dataGridView1.Rows.Add(ms.ToString("F2"), kmh.ToString("F2"), mph.ToString("F2"), sm.ToString("F2"));
                    }
                }
            }

            catch (Exception)
            {
                MessageBox.Show("Bitte nur Dezimalzahlen eingeben!");
                txt_schrittweite.Text = txt_obergrenze.Text = txt_schrittweite.Text = null;
                txt_untergrenze.Focus();
            }
        }

        private void Geschwindigkeiten_umrechnen_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("m/s");
            comboBox1.Items.Add("km/h");
            comboBox1.Items.Add("mph");
            comboBox1.Items.Add("sm");

            comboBox1.SelectedIndex = 0;
        }
    }
}
