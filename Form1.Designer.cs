namespace Geschwindigkeiten_umrechnen_GUI
{
    partial class Geschwindigkeiten_umrechnen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_untergrenze = new System.Windows.Forms.TextBox();
            this.txt_obergrenze = new System.Windows.Forms.TextBox();
            this.cmd_umrechnen = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_schrittweite = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LN_kmh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_ms = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_mph = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_sm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Untergrenze :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Obergrenze :";
            // 
            // txt_untergrenze
            // 
            this.txt_untergrenze.Location = new System.Drawing.Point(154, 45);
            this.txt_untergrenze.Name = "txt_untergrenze";
            this.txt_untergrenze.Size = new System.Drawing.Size(100, 22);
            this.txt_untergrenze.TabIndex = 1;
            // 
            // txt_obergrenze
            // 
            this.txt_obergrenze.Location = new System.Drawing.Point(154, 79);
            this.txt_obergrenze.Name = "txt_obergrenze";
            this.txt_obergrenze.Size = new System.Drawing.Size(100, 22);
            this.txt_obergrenze.TabIndex = 2;
            // 
            // cmd_umrechnen
            // 
            this.cmd_umrechnen.Location = new System.Drawing.Point(39, 235);
            this.cmd_umrechnen.Name = "cmd_umrechnen";
            this.cmd_umrechnen.Size = new System.Drawing.Size(98, 28);
            this.cmd_umrechnen.TabIndex = 5;
            this.cmd_umrechnen.Text = "&Umrechnen";
            this.cmd_umrechnen.UseVisualStyleBackColor = true;
            this.cmd_umrechnen.Click += new System.EventHandler(this.cmd_umrechnen_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(39, 299);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(98, 25);
            this.cmd_clear.TabIndex = 6;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(39, 356);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(98, 33);
            this.cmd_end.TabIndex = 7;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LN_kmh,
            this.LN_ms,
            this.LN_mph,
            this.LN_sm});
            this.dataGridView1.Location = new System.Drawing.Point(327, 45);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(444, 354);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Schrittweite:";
            // 
            // txt_schrittweite
            // 
            this.txt_schrittweite.Location = new System.Drawing.Point(154, 120);
            this.txt_schrittweite.Name = "txt_schrittweite";
            this.txt_schrittweite.Size = new System.Drawing.Size(100, 22);
            this.txt_schrittweite.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(154, 160);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 24);
            this.comboBox1.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Quelleinheit :";
            // 
            // LN_kmh
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LN_kmh.DefaultCellStyle = dataGridViewCellStyle5;
            this.LN_kmh.HeaderText = "m/s";
            this.LN_kmh.Name = "LN_kmh";
            this.LN_kmh.ReadOnly = true;
            this.LN_kmh.ToolTipText = "Meter je Sekunde";
            // 
            // LN_ms
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LN_ms.DefaultCellStyle = dataGridViewCellStyle6;
            this.LN_ms.HeaderText = "km/h";
            this.LN_ms.Name = "LN_ms";
            this.LN_ms.ReadOnly = true;
            this.LN_ms.ToolTipText = "Kilometer je Stunde";
            // 
            // LN_mph
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LN_mph.DefaultCellStyle = dataGridViewCellStyle7;
            this.LN_mph.HeaderText = "mph";
            this.LN_mph.Name = "LN_mph";
            this.LN_mph.ReadOnly = true;
            this.LN_mph.ToolTipText = "Meilen je Stunde";
            // 
            // LN_sm
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.LN_sm.DefaultCellStyle = dataGridViewCellStyle8;
            this.LN_sm.HeaderText = "sm";
            this.LN_sm.Name = "LN_sm";
            this.LN_sm.ReadOnly = true;
            this.LN_sm.ToolTipText = "Nautische Seemeilen je Stunde";
            // 
            // Geschwindigkeiten_umrechnen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 428);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.txt_schrittweite);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_umrechnen);
            this.Controls.Add(this.txt_obergrenze);
            this.Controls.Add(this.txt_untergrenze);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Geschwindigkeiten_umrechnen";
            this.Text = "Geschwindigkeiten umrechnen";
            this.Load += new System.EventHandler(this.Geschwindigkeiten_umrechnen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_untergrenze;
        private System.Windows.Forms.TextBox txt_obergrenze;
        private System.Windows.Forms.Button cmd_umrechnen;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_schrittweite;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_kmh;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_ms;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_mph;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_sm;
    }
}

